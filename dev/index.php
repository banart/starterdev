<!doctype html>

<html lang="en">
<head>
	<meta charset="utf-8">

	<title>Matteo Coletta - Starter Dev</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Matteo Coletta - Starter Dev">
	<meta name="author" content="Matteo Coletta - www.matteocoletta.it">

	<link rel="icon" type="image/png" href="images/favicon.png" />

	<link rel="stylesheet" href="css/vendor.css?v=1.0">
	<link rel="stylesheet" href="css/style.css?v=1.0">

	<!--[if lt IE 9]>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
	<![endif]-->
</head>

<body>

	<?php include('components/header.php'); ?>

	<?php
		function page($page){
			$path = 'pages/'.$page.'.php';
			if(file_exists($path)){
				include($path);
			}else{
				include('pages/404.php');
			}
		}

		if($_GET['p']){
			page($_GET['p']);
		}else{
			page('home');
		}
	?>

	<?php include('components/footer.php'); ?>

	<script src="js/vendor.js"></script>
	<script src="js/scripts.js"></script>

</body>
</html>
