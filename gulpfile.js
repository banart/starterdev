var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var stripDebug = require('gulp-strip-debug');
var size = require('gulp-size');
var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var copy = require('gulp-copy');
var livereload = require('gulp-livereload');


gulp.task('copy-vendor-style',function(){
  return gulp.src([
        './node_modules/bootstrap/dist/css/bootstrap.min.css',
        './node_modules/font-awesome/css/font-awesome.min.css',
        './dev/scss/lib/animate.css'
        ])
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('dist/css'));;
});

gulp.task('copy-vendor-scripts', function() {
    return gulp.src([
        './node_modules/jquery/dist/jquery.min.js',
        './node_modules/bootstrap/dist/js/bootstrap.min.js',
        './node_modules/bootstrap-tour/build/js/bootstrap-tour.min.js'
        ])
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('dist/js'));
});

gulp.task('copy-fonts', function() {
    return gulp.src('./node_modules/font-awesome/fonts/**/*')
        .pipe(gulp.dest('dist/fonts/'));
});

gulp.task('copy-pages', function () {
    gulp.src('dev/**/*.php')
        .pipe(gulp.dest('dist/'))
        .pipe(livereload());
});

gulp.task('images', function() {
    return gulp.src('dev/images/*')
	    .pipe(imagemin())
	    .pipe(gulp.dest('dist/images'))
        .pipe(livereload());
});

gulp.task('scripts', function() {
    gulp.src('dev/js/**/*.js')
        .pipe(stripDebug())
        .pipe(concat('scripts.js'))
        .pipe(uglify())
        .pipe(size())
        .pipe(gulp.dest('dist/js'))
        .pipe(livereload());
});

gulp.task('sass', function () {
 	return gulp.src('dev/scss/style.scss')
		.pipe(sass.sync().on('error', sass.logError))
		.pipe(gulp.dest('dist/css')).pipe(livereload());
});

gulp.task('watch', [
        'copy-vendor-style',
        'copy-vendor-scripts',
        'copy-pages',
        'copy-fonts',
        'sass',
        'images',
        'scripts'
    ], function () {

    livereload.listen();

    gulp.watch('dev/**/*.php', ['copy-pages']);
	gulp.watch('dev/scss/**/*.scss', ['sass']);
	gulp.watch('dev/images/**/*', ['images']);
	gulp.watch('dev/js/**/*', ['scripts']);
});
